package com.hcl.oxbank.service;

import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired

    private CustomerRepository customerRepository;

    public Customer datasave(CustomerDto customerDto) {
        Customer customer = new Customer();
        customer.setName(customerDto.getName());
        customer.setMobile(customerDto.getMobile());
        customer.setDob(customerDto.getDob());
        customer.setGender(customerDto.getGender());
        customer.setMaritalstatus(customerDto.getMaritalstatus());
        customer.setCreditscore(customerDto.getCreditscore());
        customer.setSalary(customerDto.getSalary());
        customer.setExpense(customerDto.getExpense());
        return customerRepository.save(customer);
    }

    public Customer getDataByID(int ID) {
        return customerRepository.findById(ID).get();
    }
}