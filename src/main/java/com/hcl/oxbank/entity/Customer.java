package com.hcl.oxbank.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcl.oxbank.Utils.ApplicationConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "customerdetails")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    int ID;

    @Column(name = ApplicationConstants.Name)
    String Name;

    @Column(name = ApplicationConstants.Mobile)
    String Mobile;

    @Column(name = ApplicationConstants.dob)
    @JsonFormat(pattern = ApplicationConstants.pattern)
    String dob;

    @Column(name = ApplicationConstants.Gender)
    String Gender;

    @Column(name = ApplicationConstants.maritalstatus)
    String maritalstatus;

    @Column(name = ApplicationConstants.creditscore)
    int creditscore;

    @Column(name = ApplicationConstants.Salary)
    int Salary;

    @Column(name = ApplicationConstants.Expense)
    int Expense;

    @OneToMany(mappedBy = "customers", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)

    private List<Loan> loans = new ArrayList<Loan>();
}